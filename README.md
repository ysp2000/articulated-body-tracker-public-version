#  Articulated Body Tracker #

Simple articulated body tracking system based on annealing particle filter algorithm.

### Suggested Reading ###

* [Deutscher J., Blake A., Reid I. - Articulated Body Motion Capture by Annealed Particle Filtering](http://www.robots.ox.ac.uk/~lav/Papers/deutscher_etal_cvpr2000/deutscher_etal_cvpr2000.pdf)

* [Lichtenauer J. - Influence of The Observation Likelihood Function on Particle Filtering Performance in Tracking Applications](http://quantum.ewi.tudelft.nl/sites/default/files/Lich04a.pdf)


### Dependencies ###

* OpenCV 3.1
* OpenMP