//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_SPACETRANSFORMATIONS_H
#define ARTICULATEDBODYTRACKER_SPACETRANSFORMATIONS_H

#include "opencv2/opencv.hpp"

using namespace cv;

class SpaceTransformations {

public:
    static Mat getTranslationMatrix(Point3f translation);
    static Mat getXRotationMatrix(float alpha);
    static Mat getYRotationMatrix(float beta);
    static Mat getZRotationMatrix(float gamma);
    static Mat getRotationMatrix(Point3f theta);
};


#endif //ARTICULATEDBODYTRACKER_SPACETRANSFORMATIONS_H
