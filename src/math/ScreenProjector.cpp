//
// Copyright (c) 2016 Sergey Esipenko
//

#include "ScreenProjector.h"

using namespace cv;

ScreenProjector::ScreenProjector(double horizontalFieldOfView, double width, double height, double zNear, double zFar) {
    horizontalFieldOfView *= CV_PI / 180.0;
    projectionMatrix = getProjectionMatrix(horizontalFieldOfView, width, height, zNear, zFar);
    screenMatrix = getScreenMatrix(width, height);
    distanceCorrection = float(width / 2.0 / tan(0.5 * horizontalFieldOfView));
}

std::vector<Point2f> ScreenProjector::getProjection(Mat &points) {
    Mat projectedPoints = points * projectionMatrix;
    normalizePoints(projectedPoints);
    return split(projectedPoints * screenMatrix);
}

Mat ScreenProjector::getProjectionMatrix(double horizontalFieldOfView, double width, double height,
                                         double zNear, double zFar) {
    const double aspectRatio = width / height;
    const double ctg = 1.0 / tan(horizontalFieldOfView * 0.5);
    const float fx = float(ctg);
    const float fy = float(ctg * aspectRatio);
    const float a = float((zFar + zNear) / (zFar - zNear));
    const float b = float(-2.0 * zFar * zNear / (zFar - zNear));
    return (Mat_<float>(4, 4) <<
            fx, 0.f, 0.f, 0.f,
            0.f, fy, 0.f, 0.f,
            0.f, 0.f, a, 1.f,
            0.f, 0.f, b, 0.f);
}

Mat ScreenProjector::getScreenMatrix(double screenWidth, double screenHeight) {
    const float halfWidth = float(0.5 * (screenWidth - 1.0));
    const float halfHeight = float(0.5 * (screenHeight - 1.0));
    return (Mat_<float>(4, 4) <<
            halfWidth, 0.f, 0.f, 0.f,
            0.f, -halfHeight, 0.f, 0.f,
            0.f, 0.f, 1.f, 0.f,
            halfWidth, halfHeight, 0.f, 1.f);
}

void ScreenProjector::normalizePoints(Mat &points) {
    for (int i = 0; i < points.rows; i++) {
        points.row(i) /= points.at<float>(i, 3);
    }
}

std::vector<Point2f> ScreenProjector::split(Mat points) {
    std::vector<Point2f> points2d;
    for (int rowIndex = 0; rowIndex < points.rows; rowIndex++) {
        points2d.push_back(Point2f(points(Range(rowIndex, rowIndex + 1), Range(0, 2))));
    }
    return points2d;
}

void ScreenProjector::reorderPoints(std::vector<Point2f> &points) {
    Point center = std::accumulate(points.begin(), points.end(), Point2f());
    center /= double(points.size());
    std::sort(points.begin(), points.end(), [center](const Point2f &point1, const Point2f &point2) {
        const bool firstLower = point1.y < center.y;
        const bool secondLower = point2.y < center.y;
        if (firstLower != secondLower) return firstLower;
        return point1.x < point2.x;
    });
}

void ScreenProjector::getBackProjection(const Mat &frame, std::vector<Point2f> &imgPoints, Mat &dst) {
    //reorderPoints(points);
    auto quadPoints = std::vector<Point2f>({Point2f(0.0f, float(dst.rows)),
                                            Point2f(0.0f, 0.0f),
                                            Point2f(float(dst.cols), 0.0f),
                                            Point2f(float(dst.cols), float(dst.rows))});
    Mat transformation = cv::getPerspectiveTransform(imgPoints, quadPoints);
    cv::warpPerspective(frame, dst, transformation, dst.size());
}
