//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_SCALARCOMBINER_H
#define ARTICULATEDBODYTRACKER_SCALARCOMBINER_H

#include <vector>

class ScalarCombiner {
public:

    virtual double combine(std::vector<double> scalars) = 0;

    virtual ~ScalarCombiner() {}
};

#endif //ARTICULATEDBODYTRACKER_SCALARCOMBINER_H
