//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_MATHUTILS_H
#define ARTICULATEDBODYTRACKER_MATHUTILS_H

class MathUtils {
public:
    template<typename T>
    static T square(T argument) { return argument * argument; }

private:
    // make static class
    MathUtils() {}
};

#endif //ARTICULATEDBODYTRACKER_MATHUTILS_H
