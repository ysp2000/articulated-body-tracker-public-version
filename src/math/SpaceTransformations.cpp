//
// Copyright (c) 2016 Sergey Esipenko
//

#include "SpaceTransformations.h"

Mat SpaceTransformations::getTranslationMatrix(Point3f translation) {
    return (Mat_<float>(4, 4) <<
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            translation.x, translation.y, translation.z, 1);
}

Mat SpaceTransformations::getXRotationMatrix(float alpha) {
    float c = cos(alpha);
    float s = sin(alpha);
    return (Mat_<float>(4, 4) <<
            1, 0, 0, 0,
            0, c, -s, 0,
            0, s, c, 0,
            0, 0, 0, 1);
}

Mat SpaceTransformations::getYRotationMatrix(float beta) {
    float c = cos(beta);
    float s = sin(beta);
    return (Mat_<float>(4, 4) <<
            c, 0, -s, 0,
            0, 1, 0, 0,
            s, 0, c, 0,
            0, 0, 0, 1);
}

Mat SpaceTransformations::getZRotationMatrix(float gamma) {
    float c = cos(gamma);
    float s = sin(gamma);
    return (Mat_<float>(4, 4) <<
            c, -s, 0, 0,
            s, c, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);
}

Mat SpaceTransformations::getRotationMatrix(Point3f theta) {
    return getXRotationMatrix(theta.x) * getYRotationMatrix(theta.y) * getZRotationMatrix(theta.z);
}
