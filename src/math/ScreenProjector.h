//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_SCREENPROJECTOR_H
#define ARTICULATEDBODYTRACKER_SCREENPROJECTOR_H

#include <opencv2/opencv.hpp>

class ScreenProjector {

public:

    ScreenProjector(double horizontalFieldOfView, double width, double height, double zNear, double zFar);

    std::vector<cv::Point2f> getProjection(cv::Mat &points);
    static void getBackProjection(const cv::Mat &frame, std::vector<cv::Point2f> &imgPoints, cv::Mat &dst);

private:
    cv::Mat projectionMatrix;
    cv::Mat screenMatrix;
    float distanceCorrection;

    static cv::Mat getProjectionMatrix(double horizontalFieldOfView, double width, double height,
                                       double zNear, double zFar);

    static cv::Mat getScreenMatrix(double screenWidth, double screenHeight);

    static void normalizePoints(cv::Mat &points);

    static std::vector<cv::Point2f> split(cv::Mat points);

    static void reorderPoints(std::vector<cv::Point2f> &points);

};


#endif //ARTICULATEDBODYTRACKER_SCREENPROJECTOR_H
