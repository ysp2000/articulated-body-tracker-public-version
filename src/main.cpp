//
//  Copyright (c) 2016 Sergey Esipenko
//

#include <iostream>
#include "tracker/params/TrackingParams.h"
#include "tracker/TrackerApp.h"

using namespace std;

int main(int argc, char *argv[]) {

    if (argc != 2) {
        std::cout << "Usage: ArticulatedBodyTracker <input configuration>" << std::endl;
        return EXIT_SUCCESS;
    }

    try {
        TrackingParams trackingParams = TrackingParams::readFromFile(argv[1]);
        TrackerApp trackerApp(trackingParams);
        trackerApp.initialize();
        trackerApp.run();
        trackerApp.release();
    } catch (std::exception &ex) {
        std::cout << "Error: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}