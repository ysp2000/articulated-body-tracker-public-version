//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_IMAGEUTILS_H
#define ARTICULATEDBODYTRACKER_IMAGEUTILS_H

#include <opencv2/opencv.hpp>

class ImageUtils {
public:

    static cv::Mat getImageGradientMagnitude(const cv::Mat &image);

    static cv::Mat zeros_like(const cv::Mat &src);

private:
    // make static class
    ImageUtils() {}
};

#endif //ARTICULATEDBODYTRACKER_IMAGEUTILS_H
