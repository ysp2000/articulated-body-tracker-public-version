//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_FILEUTILS_H
#define ARTICULATEDBODYTRACKER_FILEUTILS_H

#include <string>

class FileUtils {
public:

    static string GetPathSeparator() { return "/"; }

    static string RemoveFilename(std::string path) {
        auto lastSepPos = path.find_last_of("/\\");
        return path.substr(0, lastSepPos);
    }

private:
    // make static class
    FileUtils() {}
};


#endif //ARTICULATEDBODYTRACKER_FILEUTILS_H
