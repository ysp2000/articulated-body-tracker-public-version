//
// Copyright (c) 2016 Sergey Esipenko
//
#include "ImageUtils.h"

using namespace cv;

cv::Mat ImageUtils::getImageGradientMagnitude(const Mat &image) {

    Mat gray;
    cvtColor(image, gray, CV_BGR2GRAY);

    int scale = 1;
    int delta = 0;
    int outputDepth = CV_16S;
    int kernelSize = 3;

    Mat gradX, gradY, grad;
    Mat absGradX, absGradY;

    Sobel(gray, gradX, outputDepth, 1, 0, kernelSize, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(gradX, absGradX);

    Sobel(gray, gradY, outputDepth, 0, 1, kernelSize, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(gradY, absGradY);

    addWeighted(absGradX, 0.5, absGradY, 0.5, 0, grad);
    return grad;
}

cv::Mat ImageUtils::zeros_like(const cv::Mat &src) {
    return Mat::zeros(src.size(), src.type());
}


