//
// Copyright (c) 2016 Sergey Esipenko
//

#include "TrackerApp.h"
#include "sampling/WeightedSampler.h"
#include "dynamics/LinearDynamics.h"
#include "dynamics/RandomWalkDynamics.h"
#include "dynamics/ClippingDynamics.h"
#include "dynamics/StackedDynamics.h"
#include "sampling/MedianResultExtractor.h"
#include "evaluation/distance/DistanceCalculator.h"
#include "evaluation/distance/ChamferDistanceCalculator.h"
#include "evaluation/probability/SigmaEpsilonDistanceToProbabiltyConverter.h"
#include "evaluation/SimpleCompareToFirstEvaluator.h"
#include "tracking/particle_filter/AnnealedParticleFilterTracker.h"

using namespace std;
using namespace cv;

static const std::string TRACKER_WINDOW_NAME = "Tracking Results";
static const int THICKNESS = 2;

TrackerApp::TrackerApp(const TrackingParams &params) : params(params) {}

void TrackerApp::initialize() {
    initializeVideoCapture();
    initializeBodyToScreenMapper();
    initializeTracker();
}

void TrackerApp::run() {

    namedWindow(TRACKER_WINDOW_NAME, cv::WINDOW_AUTOSIZE);

    while (true) {
        Mat frame;
        videoCapture >> frame;
        if (!frame.data)
            break;

        preprocessFrame(frame);
        tracker->track(frame);

        if (tracker->hasPrediction()) {

            Mat trackedState;
            tracker->getPrediction(trackedState);

            auto outputImage = frame.clone();
            drawState(outputImage, trackedState);
            imshow(TRACKER_WINDOW_NAME, outputImage);
        }

        int key = waitKey(1);
        if (key == 27)
            break;
    }
}

void TrackerApp::release() {
    videoCapture.release();
}

void TrackerApp::initializeVideoCapture() {

    videoCapture.open(params.getInputVideoFile());

    if (!videoCapture.isOpened())
        throw std::runtime_error("Cannot open provided video file");

    videoCapture >> initialFrame;

    if (initialFrame.empty())
        throw std::runtime_error("Video file is empty or have invalid format");

    preprocessFrame(initialFrame);
}

void TrackerApp::initializeBodyToScreenMapper() {

    const auto &bodyParams = params.getBodyParams();
    const auto &cameraParams = params.getCameraParams();

    auto screenProjector = make_shared<ScreenProjector>(cameraParams.getHorizontalFieldOfView(),
                                                        initialFrame.size().width,
                                                        initialFrame.size().height,
                                                        cameraParams.getZNear(),
                                                        cameraParams.getZFar());

    bodyToStateMapper = make_shared<BodyToStateMapper>(params.getDynamicsModelOrder(), bodyParams);
    bodyToScreenMapper = make_shared<BodyToScreenMapper>(bodyParams, bodyToStateMapper, screenProjector);
}

void TrackerApp::initializeTracker() {

    auto sampler = initializeSampler();
    auto dynamics = initializeDynamics();
    auto evaluator = initializeEvaluator(initialFrame);
    auto resultExtractor = initializeResultExtractor();

    tracker = make_unique<AnnealedParticleFilterTracker>(sampler, dynamics, evaluator, resultExtractor,
                                                         params.getNumSamples(),
                                                         params.getNumAnnealingItterations());

    std::vector<Sample> initialDistribution;
    initialDistribution.emplace_back(bodyToStateMapper->getInitialState(), 1.0);

    tracker->initialize(params, initialDistribution);
}

std::unique_ptr<Sampler> TrackerApp::initializeSampler() {
    return make_unique<WeightedSampler>();
}

std::unique_ptr<Dynamics> TrackerApp::initializeDynamics() {

    const auto &stateParams = params.getStateParams();

    unique_ptr<Dynamics> linearDynamics = make_unique<LinearDynamics>();
    unique_ptr<Dynamics> randomWalkDynamics = make_unique<RandomWalkDynamics>(stateParams.getStandardDeviation());
    unique_ptr<Dynamics> clippingDynamics = make_unique<ClippingDynamics>(stateParams.getStateParamsMin(),
                                                                          stateParams.getStateParamsMax());

    StackedDynamics *stackedDynamics = new StackedDynamics();
    stackedDynamics->addToDynamicsChain(linearDynamics);
    stackedDynamics->addToDynamicsChain(randomWalkDynamics);
    stackedDynamics->addToDynamicsChain(clippingDynamics);
    return unique_ptr<Dynamics>(stackedDynamics);
}

std::unique_ptr<Evaluator> TrackerApp::initializeEvaluator(const cv::Mat &initialFrame) {

    Mat initialState = bodyToStateMapper->getInitialState();

    unique_ptr<DistanceCalculator> distanceCalculator = make_unique<ChamferDistanceCalculator>(bodyToScreenMapper);

    unique_ptr<DistanceToProbabilityConverter> distanceToProbabilityConverter =
            make_unique<SigmaEpsilonDistanceToProbabiltyConverter>(1.5, 0.001);

    return make_unique<SimpleCompareToFirstEvaluator>(initialFrame, initialState, distanceCalculator,
                                                      distanceToProbabilityConverter);
}

std::unique_ptr<ResultExtractor> TrackerApp::initializeResultExtractor() {
    return make_unique<MedianResultExtractor>();
}

void TrackerApp::drawState(cv::Mat &frame, const cv::Mat &state, double alpha) {
    Mat buffer = Mat::zeros(frame.size(), frame.type());

    std::vector<std::vector<Point2f>> limbsVerts;
    bodyToScreenMapper->getLimbsVertices(state, limbsVerts);

    const auto &limbs = params.getBodyParams().getLimbs();

    for (int idx = 0; idx < limbsVerts.size(); idx++) {
        drawLimb(buffer, limbsVerts[idx], limbs[idx].color);
    }

    addWeighted(frame, 1.0, buffer, alpha, 0.0, frame);
}

void TrackerApp::drawLimb(cv::Mat &frame, const std::vector<cv::Point2f> &verts, const cv::Scalar &color) {
    for (size_t vertexIndex = 0; vertexIndex < verts.size(); vertexIndex++) {
        size_t nextVertexIndex = (vertexIndex + 1) % verts.size();
        line(frame, verts[vertexIndex], verts[nextVertexIndex], color, THICKNESS);
    }
}

void TrackerApp::preprocessFrame(cv::Mat &frame) {
    pyrDown(frame, frame);
}
