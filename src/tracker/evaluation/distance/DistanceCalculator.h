//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_DISTANCECALCULATOR_H
#define ARTICULATEDBODYTRACKER_DISTANCECALCULATOR_H

#include <memory>
#include <opencv2/opencv.hpp>
#include "../../body/BodyToScreenMapper.h"

class DistanceCalculator {
public:

    DistanceCalculator(const std::shared_ptr<BodyToScreenMapper> &bodyToScreenMapper)
            : bodyToScreenMapper(bodyToScreenMapper) {}

    virtual void setOrigin(const cv::Mat& frame, const cv::Mat& state) = 0;

    virtual void observe(const cv::Mat &frame) = 0;

    virtual std::vector<double> getDistanceToOrigin(cv::Mat& state) = 0;

    virtual ~DistanceCalculator() {}

protected:
    std::shared_ptr<BodyToScreenMapper> bodyToScreenMapper;
};

#endif //ARTICULATEDBODYTRACKER_DISTANCECALCULATOR_H
