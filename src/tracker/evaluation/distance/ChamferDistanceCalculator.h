//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_CHAMFERDISTANCECALCULATOR_H
#define ARTICULATEDBODYTRACKER_CHAMFERDISTANCECALCULATOR_H

#include <memory>
#include <opencv2/opencv.hpp>
#include "DistanceCalculator.h"

class ChamferDistanceCalculator : public DistanceCalculator {
public:

    ChamferDistanceCalculator(const std::shared_ptr<BodyToScreenMapper> &bodyToScreenMapper,
                              int binarizationThreshold = 90);

    void setOrigin(const cv::Mat &frame, const cv::Mat &state) override;

    void observe(const cv::Mat &frame) override;

    std::vector<double> getDistanceToOrigin(cv::Mat &state) override;

private:

    int binarizationThreshold;

    std::vector<cv::Mat> initialLimbsContours;
    std::vector<cv::Mat> initialLimbsDistMap;
    cv::Mat currentFrame;

    cv::Mat prepareFrame(const cv::Mat &frame);

    double chamferDistance(cv::Mat& originDistMap, cv::Mat& originContours, cv::Mat& contours);

};


#endif //ARTICULATEDBODYTRACKER_CHAMFERDISTANCECALCULATOR_H
