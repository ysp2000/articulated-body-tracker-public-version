//
// Copyright (c) 2016 Sergey Esipenko
//

#include "ChamferDistanceCalculator.h"
#include "../../../utils/ImageUtils.h"

using namespace cv;

ChamferDistanceCalculator::ChamferDistanceCalculator(const std::shared_ptr<BodyToScreenMapper> &bodyToScreenMapper,
                                                     int binarizationThreshold) :
        DistanceCalculator(bodyToScreenMapper), binarizationThreshold(binarizationThreshold) {}

void ChamferDistanceCalculator::setOrigin(const cv::Mat &frame, const cv::Mat &state) {

    Mat grad = ImageUtils::getImageGradientMagnitude(frame);

    Mat binarizedGrad;
    Mat distanceMap;
    Mat contours;

    threshold(grad, binarizedGrad, binarizationThreshold, 255, CV_THRESH_BINARY_INV);
    threshold(grad, contours, binarizationThreshold, 255, CV_THRESH_BINARY);

    distanceTransform(binarizedGrad, distanceMap, CV_DIST_L2, 3, CV_32F);

    contours.convertTo(contours, CV_32F, 1.0 / 255.0);

    bodyToScreenMapper->extractLimbsImages(initialLimbsDistMap, distanceMap, state);
    bodyToScreenMapper->extractLimbsImages(initialLimbsContours, contours, state);
}

void ChamferDistanceCalculator::observe(const Mat &frame) {
    currentFrame = prepareFrame(frame);
}

std::vector<double> ChamferDistanceCalculator::getDistanceToOrigin(cv::Mat &state) {

    std::vector<Mat> limbsContours = bodyToScreenMapper->getLimbsImages(currentFrame, state);
    std::vector<double> distances;

    for (int limbIndex = 0; limbIndex < limbsContours.size(); limbIndex++) {

        const double distance = chamferDistance(initialLimbsDistMap[limbIndex],
                                                initialLimbsContours[limbIndex],
                                                limbsContours[limbIndex]);

        distances.push_back(distance);
    }

    return distances;
}

cv::Mat ChamferDistanceCalculator::prepareFrame(const Mat &frame) {
    Mat grad = ImageUtils::getImageGradientMagnitude(frame);
    threshold(grad, grad, binarizationThreshold, 255, CV_THRESH_BINARY);
    grad.convertTo(grad, CV_32F, 1.0 / 255.0);
    return grad;
}

double ChamferDistanceCalculator::chamferDistance(cv::Mat &originDistMap, cv::Mat &originContours, cv::Mat &contours) {

    Mat distances = originDistMap.mul(contours);

    const double numEdgePixel = std::max(0.1, norm(originContours, NORM_L1));

    const double chamferDistance = norm(distances, NORM_L1) / numEdgePixel;
    const double contourDist = norm(originContours, contours, NORM_L2) / numEdgePixel;

    return chamferDistance * contourDist;
}




