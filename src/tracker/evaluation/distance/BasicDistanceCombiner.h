//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_BASICDISTANCECOMBINER_H
#define ARTICULATEDBODYTRACKER_BASICDISTANCECOMBINER_H

#include "../../../math/ScalarCombiner.h"

class BasicDistanceCombiner : public ScalarCombiner {
public:

    double combine(std::vector<double> scalars) override {
        double result = 0.0;
        double factor = 1.0;

        for (auto &scalar : scalars) {
            result += factor * scalar;
            factor += 2.0;
        }

        return result;
    }

};

#endif //ARTICULATEDBODYTRACKER_BASICDISTANCECOMBINER_H
