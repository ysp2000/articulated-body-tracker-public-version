//// Copyright (c) 2016 Sergey Esipenko

// Copyright (c) 2016 Sergey Esipenko
//

#include "SigmaEpsilonDistanceToProbabiltyConverter.h"
#include "../../../math/MathUtils.h"
#include <cmath>

SigmaEpsilonDistanceToProbabiltyConverter::SigmaEpsilonDistanceToProbabiltyConverter(double sigma, double epsilon)
        : sigma(sigma), epsilon(epsilon) {}

double SigmaEpsilonDistanceToProbabiltyConverter::convert(double distance) {
    return epsilon + std::exp(-0.5 * MathUtils::square(distance / sigma));
}
