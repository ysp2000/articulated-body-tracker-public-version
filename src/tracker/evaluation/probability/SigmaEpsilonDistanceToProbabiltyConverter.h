//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_SIGMAEPSILONDISTANCETOPROBABILTYCONVERTER_H
#define ARTICULATEDBODYTRACKER_SIGMAEPSILONDISTANCETOPROBABILTYCONVERTER_H


#include "DistanceToProbabilityConverter.h"

/**
 * Suggested reading: Lichtenauer J. - Influence of The Observation Likelihood Function on Particle Filtering
 * Performance in Tracking Applications [http://quantum.ewi.tudelft.nl/sites/default/files/Lich04a.pdf]
 */
class SigmaEpsilonDistanceToProbabiltyConverter : public DistanceToProbabilityConverter {

public:
    SigmaEpsilonDistanceToProbabiltyConverter(double sigma, double epsilon);

private:
    double convert(double distance) override;

private:
    double sigma;
    double epsilon;
};


#endif //ARTICULATEDBODYTRACKER_SIGMAEPSILONDISTANCETOPROBABILTYCONVERTER_H
