//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_BASICPROBABILITYCOMBINER_H
#define ARTICULATEDBODYTRACKER_BASICPROBABILITYCOMBINER_H

#include "../../../math/ScalarCombiner.h"

class BasicProbabilityCombiner : public ScalarCombiner {
public:

    double combine(std::vector<double> scalars) override {
        double result = 1.0;

        for (auto &scalar : scalars) {
            result *= scalar;
        }

        return result;
    }

};

#endif //ARTICULATEDBODYTRACKER_BASICPROBABILITYCOMBINER_H
