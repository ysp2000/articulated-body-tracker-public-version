//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_DISTANCETOPROBABILTYCONVERTER_H
#define ARTICULATEDBODYTRACKER_DISTANCETOPROBABILTYCONVERTER_H

#include <vector>

class DistanceToProbabilityConverter {
public:

    virtual double convert(double distance) = 0;

    virtual ~DistanceToProbabilityConverter() {}

    std::vector<double> convert(std::vector<double> &distances) {
        std::vector<double> probabilities;
        for (auto &distance : distances) {
            probabilities.push_back(convert(distance));
        }
        return probabilities;
    }

};


#endif //ARTICULATEDBODYTRACKER_DISTANCETOPROBABILTYCONVERTER_H
