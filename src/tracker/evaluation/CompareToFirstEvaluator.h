//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_COMPARETOFIRSTEVALUATOR_H
#define ARTICULATEDBODYTRACKER_COMPARETOFIRSTEVALUATOR_H

#include <opencv2/opencv.hpp>

#include "Evaluator.h"

class CompareToFirstEvaluator : public Evaluator {
public:
    CompareToFirstEvaluator(const cv::Mat &initialFrame, const cv::Mat &initialState);

    virtual void observe(const cv::Mat &frame) override;

    virtual void accept(cv::Mat &state) override;

protected:
    cv::Mat initialFrame;
    cv::Mat initialState;
    cv::Mat lastFrame;
    cv::Mat lastState;
    cv::Mat observedFrame;
};

#endif //ARTICULATEDBODYTRACKER_COMPARETOFIRSTEVALUATOR_H
