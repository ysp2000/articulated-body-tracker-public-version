//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_SIMPLECOMPARETOFIRSTEVALUATOR_H
#define ARTICULATEDBODYTRACKER_SIMPLECOMPARETOFIRSTEVALUATOR_H

#include <memory>
#include "CompareToFirstEvaluator.h"
#include "distance/DistanceCalculator.h"
#include "../../math/ScalarCombiner.h"
#include "probability/DistanceToProbabilityConverter.h"
#include "distance/BasicDistanceCombiner.h"
#include "probability/BasicProbabilityCombiner.h"

class SimpleCompareToFirstEvaluator : public CompareToFirstEvaluator {
public:

    SimpleCompareToFirstEvaluator(const cv::Mat &initialFrame, const cv::Mat &initialState,
                                  std::unique_ptr<DistanceCalculator> &distanceCalculator,
                                  std::unique_ptr<DistanceToProbabilityConverter> &distanceToProbabilityConverter);

    void observe(const cv::Mat &frame) override;

    double evaluate(cv::Mat &state) override;

private:
    std::unique_ptr<DistanceCalculator> distanceCalculator_;
    BasicDistanceCombiner distanceCombiner;
    BasicProbabilityCombiner probabilityCombiner;
    std::unique_ptr<DistanceToProbabilityConverter> distanceToProbabilityConverter;
};

#endif //ARTICULATEDBODYTRACKER_SIMPLECOMPARETOFIRSTEVALUATOR_H
