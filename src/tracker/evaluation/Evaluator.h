//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_EVALUATOR_H
#define ARTICULATEDBODYTRACKER_EVALUATOR_H

#include "opencv2/opencv.hpp"

class Evaluator {
public:
    virtual void observe(const cv::Mat &frame) = 0;

    virtual double evaluate(cv::Mat &state) = 0;

    virtual void accept(cv::Mat &state) = 0;

    virtual ~Evaluator() {}
};

#endif //ARTICULATEDBODYTRACKER_EVALUATOR_H
