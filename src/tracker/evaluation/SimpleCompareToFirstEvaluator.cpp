//
// Copyright (c) 2016 Sergey Esipenko
//

#include "SimpleCompareToFirstEvaluator.h"

SimpleCompareToFirstEvaluator::SimpleCompareToFirstEvaluator(const cv::Mat &initialFrame, const cv::Mat &initialState,
                                                             std::unique_ptr<DistanceCalculator> &distanceCalculator,
                                                             std::unique_ptr<DistanceToProbabilityConverter> &distanceToProbabilityConverter)
        : CompareToFirstEvaluator(initialFrame, initialState), distanceCalculator_(std::move(distanceCalculator)),
          distanceCombiner(distanceCombiner), probabilityCombiner(probabilityCombiner),
          distanceToProbabilityConverter(std::move(distanceToProbabilityConverter)) {
    distanceCalculator_->setOrigin(initialFrame, initialState);
}

void SimpleCompareToFirstEvaluator::observe(const cv::Mat &frame) {
    CompareToFirstEvaluator::observe(frame);
    distanceCalculator_->observe(frame);
}

double SimpleCompareToFirstEvaluator::evaluate(cv::Mat &state) {
    std::vector<double> distances = distanceCalculator_->getDistanceToOrigin(state);
    const double distance = distanceCombiner.combine(distances);
    return distanceToProbabilityConverter->convert(distance);
}
