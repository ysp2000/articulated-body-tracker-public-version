//
// Copyright (c) 2016 Sergey Esipenko
//

#include "CompareToFirstEvaluator.h"

CompareToFirstEvaluator::CompareToFirstEvaluator(const cv::Mat &initialFrame, const cv::Mat &initialState)
        : initialFrame(initialFrame), initialState(initialState) {}

void CompareToFirstEvaluator::observe(const cv::Mat &frame) {
    observedFrame = frame.clone();
}

void CompareToFirstEvaluator::accept(cv::Mat &state) {
    lastFrame = observedFrame;
    lastState = state;
}
