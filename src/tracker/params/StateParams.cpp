//
// Copyright (c) 2016 Sergey Esipenko
//

#include "StateParams.h"

StateParams::StateParams(const Mat &standardDeviation, const Mat &stateParamsMin, const Mat &stateParamsMax)
        : standardDeviation(standardDeviation), stateParamsMin(stateParamsMin), stateParamsMax(stateParamsMax) {}

const Mat &StateParams::getStandardDeviation() const {
    return standardDeviation;
}

const Mat &StateParams::getStateParamsMin() const {
    return stateParamsMin;
}

const Mat &StateParams::getStateParamsMax() const {
    return stateParamsMax;
}
