//
// Copyright (c) 2016 Sergey Esipenko
//

#include "TrackingParams.h"
#include "../../utils/FileUtils.h"

TrackingParams::TrackingParams(const string &inputVideoFile, int numSamples, int numAnnealingIterations,
                               int dynamicsModelOrder, const CameraParams &cameraParams, const BodyParams &bodyParams,
                               const StateParams &stateParams) : inputVideoFile(inputVideoFile), numSamples(numSamples),
                                                                 numAnnealingItterations(numAnnealingIterations),
                                                                 dynamicsModelOrder(dynamicsModelOrder),
                                                                 cameraParams(cameraParams), bodyParams(bodyParams),
                                                                 stateParams(stateParams) {}

TrackingParams TrackingParams::readFromFile(string paramsFile) {

    FileStorage params(paramsFile, FileStorage::READ);

    if (!params.isOpened())
        throw std::runtime_error("Cannot open configuration file!");

    string videoFile;
    readRequiredParameter(params, PARAM_VIDEO, videoFile);
    string paramsPath = FileUtils::RemoveFilename(paramsFile);
    string videoFullPath = paramsPath + FileUtils::GetPathSeparator() + videoFile;

    int numSamples;
    readRequiredParameter(params, PARAM_NUM_SAMPLES, numSamples);

    int numAnnealingIterations;
    readRequiredParameter(params, PARAM_NUM_ANNEALING_ITERATIONS, numAnnealingIterations);

    int dynamicsModelOrder;
    readRequiredParameter(params, PARAM_DYNAMIC_MODEL_ORDER, dynamicsModelOrder);

    CameraParams cameraParams = readCameraParams(params);
    BodyParams bodyParams = readBodyParams(params);
    StateParams stateParams = readStateParams(params, dynamicsModelOrder);

    params.release();

    return TrackingParams(videoFullPath, numSamples, numAnnealingIterations, dynamicsModelOrder, cameraParams,
                          bodyParams, stateParams);
}

CameraParams TrackingParams::readCameraParams(FileStorage &params) {

    double horizontalFieldOfView;
    double zNear;
    double zFar;

    readRequiredParameter(params, PARAM_HFOV, horizontalFieldOfView);
    readRequiredParameter(params, PARAM_ZNEAR, zNear);
    readRequiredParameter(params, PARAM_ZFAR, zFar);

    return CameraParams(horizontalFieldOfView, zNear, zFar);
}

BodyParams TrackingParams::readBodyParams(FileStorage &params) {

    vector<Limb> limbs;
    vector<Point3f> initialPositions;
    vector<Point3f> initialOrientations;

    FileNode limbParams = params[PARAM_LIMBS];

    for (auto it = limbParams.begin(); it != limbParams.end(); ++it) {

        auto limb = (*it);

        int index;
        readRequiredParameter(limb, PARAM_LIMB_INDEX, index);

        std::string name;
        readRequiredParameter(limb, PARAM_LIMB_NAME, name);

        int parent;
        readRequiredParameter(limb, PARAM_LIMB_PARENT, parent);

        std::vector<int> color;
        readRequiredParameter(limb, PARAM_LIMB_COLOR, color);

        float length;
        readRequiredParameter(limb, PARAM_LIMB_LENGTH, length);

        float radius;
        readRequiredParameter(limb, PARAM_LIMB_RADIUS, radius);

        std::vector<float> position;
        readRequiredParameter(limb, PARAM_LIMB_POSITION, position);

        std::vector<float> orientation;
        readRequiredParameter(limb, PARAM_LIMB_ORIENTATION, orientation);

        limbs.push_back(Limb(index, parent, length, radius, CV_RGB(color[0], color[1], color[2])));
        initialPositions.push_back(Point3f(position[0], position[1], position[2]));
        initialOrientations.push_back(Point3f(orientation[0], orientation[1], orientation[2]));
    }

    if (limbs.empty())
        throw std::runtime_error("Limbs list is empty!");

    return BodyParams(limbs, initialPositions, initialOrientations);
}

StateParams TrackingParams::readStateParams(FileStorage &params, int dynamicsModelOrder) {

    Mat standardDeviation;
    Mat stateParamsMinValue;
    Mat stateParamsMaxValue;

    float stdDevModifier;
    readParameter(params, PARAM_STATE_GLOBAL_STDDEV_MODIFIER, stdDevModifier, 1.0f);

    FileNode limbParams = params[PARAM_LIMBS];

    for (auto it = limbParams.begin(); it != limbParams.end(); ++it) {

        auto limb = (*it);

        vector<float> stdDev;
        readRequiredParameter(limb, PARAM_LIMB_STATE_STDDEV, stdDev);

        vector<float> minVal;
        readRequiredParameter(limb, PARAM_LIMB_STATE_MINVAL, minVal);

        vector<float> maxVal;
        readRequiredParameter(limb, PARAM_LIMB_STATE_MAXVAL, maxVal);

        Mat limbStdDev = Mat(stdDev).reshape(1, 2).rowRange(Range(0, dynamicsModelOrder + 1));
        Mat limbMinVal = Mat(minVal).reshape(1, 2).rowRange(Range(0, dynamicsModelOrder + 1));
        Mat limbMaxVal = Mat(maxVal).reshape(1, 2).rowRange(Range(0, dynamicsModelOrder + 1));

        limbStdDev *= stdDevModifier;

        if (standardDeviation.rows == 0) {
            limbStdDev.copyTo(standardDeviation);
            limbMinVal.copyTo(stateParamsMinValue);
            limbMaxVal.copyTo(stateParamsMaxValue);
        } else {
            hconcat(standardDeviation, limbStdDev, standardDeviation);
            hconcat(stateParamsMinValue, limbMinVal, stateParamsMinValue);
            hconcat(stateParamsMaxValue, limbMaxVal, stateParamsMaxValue);
        }
    }

    return StateParams(standardDeviation, stateParamsMinValue, stateParamsMaxValue);
}

const string &TrackingParams::getInputVideoFile() const {
    return inputVideoFile;
}

int TrackingParams::getDynamicsModelOrder() const {
    return dynamicsModelOrder;
}

const CameraParams &TrackingParams::getCameraParams() const {
    return cameraParams;
}

const BodyParams &TrackingParams::getBodyParams() const {
    return bodyParams;
}

const StateParams &TrackingParams::getStateParams() const {
    return stateParams;
}

int TrackingParams::getNumSamples() const {
    return numSamples;
}

int TrackingParams::getNumAnnealingItterations() const {
    return numAnnealingItterations;
}

template <typename TNode, typename TParam>
bool TrackingParams::readRequiredParameter(TNode &params, const char *paramName, TParam &param, bool throwException) {

    const auto &node = params[paramName];

    if (node.empty()) {
        if (throwException)
            throw std::runtime_error("Required parameter '"s + paramName + "' not found!"s);
        return false;
    }

    node >> param;
    return true;
}

template<typename TNode, typename TParam>
bool TrackingParams::readParameter(TNode &params, const char *paramName, TParam &param, TParam defaultValue) {

    const auto &node = params[paramName];

    if (node.empty()) {
        param = defaultValue;
        return false;
    }

    node >> param;
    return 0;
}
