//
// Copyright (c) 2016 Sergey Esipenko
//

#include "BodyParams.h"

using  namespace std;
using  namespace cv;

BodyParams::BodyParams(const vector<Limb> &limbs, const vector<Point3f> &initialPositions,
                       const vector<Point3f> &initialOrientations) : limbs(limbs), initialPositions(initialPositions),
                                                                     initialOrientations(initialOrientations) {}

const vector<Limb> &BodyParams::getLimbs() const {
    return limbs;
}

const vector<Point3f> &BodyParams::getInitialPositions() const {
    return initialPositions;
}

const vector<Point3f> &BodyParams::getInitialOrientations() const {
    return initialOrientations;
}
