//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_BODYPARAMS_H
#define ARTICULATEDBODYTRACKER_BODYPARAMS_H

#include <vector>
#include <opencv2/opencv.hpp>
#include "../body/Limb.h"



class BodyParams {

public:
    BodyParams(const std::vector<Limb> &limbs, const std::vector<cv::Point3f> &initialPositions,
               const std::vector<cv::Point3f> &initialOrientations);

    const std::vector<Limb> &getLimbs() const;

    const std::vector<cv::Point3f> &getInitialPositions() const;

    const std::vector<cv::Point3f> &getInitialOrientations() const;

private:

    std::vector<Limb> limbs;
    std::vector<cv::Point3f> initialPositions;
    std::vector<cv::Point3f> initialOrientations;

};


#endif //ARTICULATEDBODYTRACKER_BODYPARAMS_H
