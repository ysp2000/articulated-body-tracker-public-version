//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_CAMERAPARAMS_H
#define ARTICULATEDBODYTRACKER_CAMERAPARAMS_H

#include <opencv2/opencv.hpp>

class CameraParams {

public:
    CameraParams(double horizontalFieldOfView, double zNear, double zFar);

    double getHorizontalFieldOfView() const;

    double getZNear() const;

    double getZFar() const;

private:
    double horizontalFieldOfView;
    double zNear;
    double zFar;
};


#endif //ARTICULATEDBODYTRACKER_CAMERAPARAMS_H
