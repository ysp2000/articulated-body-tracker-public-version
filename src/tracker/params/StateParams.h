//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_STATEPARAMS_H
#define ARTICULATEDBODYTRACKER_STATEPARAMS_H

#include <opencv2/opencv.hpp>

using namespace cv;

class StateParams {

public:

    StateParams(const Mat &standardDeviation, const Mat &stateParamsMin,
                const Mat &stateParamsMax);

    const Mat &getStandardDeviation() const;

    const Mat &getStateParamsMin() const;

    const Mat &getStateParamsMax() const;

private:
    Mat standardDeviation;
    Mat stateParamsMin;
    Mat stateParamsMax;
};


#endif //ARTICULATEDBODYTRACKER_STATEPARAMS_H
