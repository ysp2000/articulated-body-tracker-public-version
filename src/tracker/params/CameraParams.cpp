//
// Copyright (c) 2016 Sergey Esipenko
//

#include "CameraParams.h"

CameraParams::CameraParams(double horizontalFieldOfView, double zNear, double zFar) : horizontalFieldOfView(
        horizontalFieldOfView), zNear(zNear), zFar(zFar) {}

double CameraParams::getHorizontalFieldOfView() const {
    return horizontalFieldOfView;
}

double CameraParams::getZNear() const {
    return zNear;
}

double CameraParams::getZFar() const {
    return zFar;
}
