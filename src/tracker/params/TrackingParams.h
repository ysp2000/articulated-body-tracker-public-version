//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_TRACKINGPARAMS_H
#define ARTICULATEDBODYTRACKER_TRACKINGPARAMS_H

#include <string>
#include "CameraParams.h"
#include "BodyParams.h"
#include "StateParams.h"

using namespace std;

class TrackingParams {

public:

    TrackingParams(const string &inputVideoFile, int numSamples, int numAnnealingItterations, int dynamicsModelOrder,
                   const CameraParams &cameraParams, const BodyParams &bodyParams, const StateParams &stateParams);

    static TrackingParams readFromFile(string paramsFile);

    const string &getInputVideoFile() const;

    int getDynamicsModelOrder() const;

    const CameraParams &getCameraParams() const;

    const BodyParams &getBodyParams() const;

    const StateParams &getStateParams() const;

    int getNumSamples() const;

    int getNumAnnealingItterations() const;

private:

    static constexpr const char *PARAM_VIDEO = "video";
    static constexpr const char *PARAM_NUM_SAMPLES = "num-samples";
    static constexpr const char *PARAM_NUM_ANNEALING_ITERATIONS = "annealing-iterations";
    static constexpr const char *PARAM_DYNAMIC_MODEL_ORDER = "dynamics-model-order";

    static constexpr const char *PARAM_HFOV = "hFov";
    static constexpr const char *PARAM_ZNEAR = "zNear";
    static constexpr const char *PARAM_ZFAR = "zFar";

    static constexpr const char *PARAM_LIMBS = "limbs";
    static constexpr const char *PARAM_LIMB_INDEX = "index";
    static constexpr const char *PARAM_LIMB_NAME = "name";
    static constexpr const char *PARAM_LIMB_PARENT = "parent";
    static constexpr const char *PARAM_LIMB_COLOR = "color";
    static constexpr const char *PARAM_LIMB_LENGTH = "length";
    static constexpr const char *PARAM_LIMB_RADIUS = "radius";
    static constexpr const char *PARAM_LIMB_POSITION = "position";
    static constexpr const char *PARAM_LIMB_ORIENTATION = "orientation";

    static constexpr const char *PARAM_STATE_GLOBAL_STDDEV_MODIFIER = "std-dev-modifier";
    static constexpr const char *PARAM_LIMB_STATE_STDDEV = "std-dev";
    static constexpr const char *PARAM_LIMB_STATE_MINVAL = "min-val";
    static constexpr const char *PARAM_LIMB_STATE_MAXVAL = "max-val";


    string inputVideoFile;
    int numSamples;
    int numAnnealingItterations;
    int dynamicsModelOrder;
    CameraParams cameraParams;
    BodyParams bodyParams;
    StateParams stateParams;

    static CameraParams readCameraParams(FileStorage &params);

    static BodyParams readBodyParams(FileStorage &params);

    static StateParams readStateParams(FileStorage &params, int dynamicsModelOrder);

    template <typename TNode, typename TParam>
    static bool readRequiredParameter(TNode &params, const char *paramName, TParam &param, bool throwException = true);

    template <typename TNode, typename TParam>
    static bool readParameter(TNode &params, const char *paramName, TParam &param, TParam defaultValue);
};


#endif //ARTICULATEDBODYTRACKER_TRACKINGPARAMS_H
