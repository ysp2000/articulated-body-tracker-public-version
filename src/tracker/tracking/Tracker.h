//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_TRACKER_H
#define ARTICULATEDBODYTRACKER_TRACKER_H

#include <vector>
#include <opencv2/opencv.hpp>
#include "../params/TrackingParams.h"
#include "../sampling/Sample.h"

class Tracker {
public:

    virtual void initialize(const TrackingParams &trackingParams, const std::vector<Sample> &initialDistribution) = 0;

    virtual void track(const cv::Mat &frame) = 0;

    virtual bool hasPrediction() = 0;

    virtual bool getPrediction(cv::Mat &prediction) = 0;

    virtual ~Tracker() {}

};


#endif //ARTICULATEDBODYTRACKER_TRACKER_H
