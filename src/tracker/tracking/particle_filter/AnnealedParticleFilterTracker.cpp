//
// Copyright (c) 2016 Sergey Esipenko
//

#include "AnnealedParticleFilterTracker.h"
#include "../../dynamics/RandomWalkDynamics.h"


AnnealedParticleFilterTracker::AnnealedParticleFilterTracker(std::unique_ptr<Sampler> &sampler,
                                                             std::unique_ptr<Dynamics> &dynamics,
                                                             std::unique_ptr<Evaluator> &evaluator,
                                                             std::unique_ptr<ResultExtractor> &resultExtractor,
                                                             int numSamples,
                                                             int numAnnealingIterations)
        : sampler(std::move(sampler)), dynamics(std::move(dynamics)), evaluator(std::move(evaluator)),
          resultExtractor(std::move(resultExtractor)), numSamples(numSamples),
          numAnnealingIterations(numAnnealingIterations), hasPredictionFlag(false) {}

void AnnealedParticleFilterTracker::initialize(const TrackingParams &trackingParams,
                                               const std::vector<Sample> &initialDistribution) {

    driftDynamics = std::make_unique<RandomWalkDynamics>(trackingParams.getStateParams().getStandardDeviation());

    distribution.insert(distribution.begin(), initialDistribution.begin(), initialDistribution.end());
}

void AnnealedParticleFilterTracker::track(const cv::Mat &frame) {

    // 1. Resampling
    sampler->sampleDistribution(distribution, numSamples, resampledDistribution);

    // 2. Sample propagation
    propagate(resampledDistribution, propagatedDistribution);

    // 3. Samples weights calculation
    evaluator->observe(frame);

    for (int annealingIteration = 1; annealingIteration <= numAnnealingIterations; annealingIteration++) {

        const double annealingFunctionExp = numAnnealingIterations + 1 - annealingIteration;

        #pragma omp parallel for
        for (size_t sampleIndex = 0; sampleIndex < propagatedDistribution.size(); sampleIndex++) {
            auto &sample = propagatedDistribution[sampleIndex];
            const double normalSampleWeight = evaluator->evaluate(sample.state);
            const double annealedSampleWeight =std::pow(normalSampleWeight, annealingFunctionExp);
            sample.weight = annealedSampleWeight;
        }

        normalize(propagatedDistribution);

        distribution = propagatedDistribution; // copy propagatedDistribution to distribution

        if (annealingIteration < numAnnealingIterations) {
            sampler->sampleDistribution(distribution, numSamples, resampledDistribution);
            drift(resampledDistribution, propagatedDistribution);
        }
    }

    prediction = resultExtractor->extract(distribution);
    evaluator->accept(prediction);
    hasPredictionFlag = true;
}

bool AnnealedParticleFilterTracker::hasPrediction() {
    return hasPredictionFlag;
}

bool AnnealedParticleFilterTracker::getPrediction(cv::Mat &prediction) {

    if (hasPredictionFlag) {
        prediction = this->prediction;
    }

    return hasPredictionFlag;
}

void AnnealedParticleFilterTracker::propagate(const vector<Sample> &currentDistribution,
                                              vector<Sample> &updatedDistribution) {
    applyDynamics(currentDistribution, updatedDistribution, dynamics.get());
}

void AnnealedParticleFilterTracker::drift(const vector<Sample> &currentDistribution,
                                              vector<Sample> &updatedDistribution) {
    applyDynamics(currentDistribution, updatedDistribution, driftDynamics.get());
}

void AnnealedParticleFilterTracker::applyDynamics(const vector<Sample> &currentDistribution,
                                              vector<Sample> &updatedDistribution, Dynamics *customDynamics) {
    updatedDistribution.clear();

    for (auto &sample : currentDistribution) {
        Mat currentState = sample.state;
        auto updatedState = customDynamics->simulate(currentState);
        auto updatedWeight = sample.weight;
        updatedDistribution.emplace_back(updatedState, updatedWeight);
    }
}

void AnnealedParticleFilterTracker::normalize(vector<Sample> &distribution) {

    const double weightsSum = std::accumulate(distribution.begin(), distribution.end(), 0.0,
                                              [](double sum, Sample &sample) {
        return sum + sample.weight;
    });

    for (auto &sample : distribution) {
        sample.weight /= weightsSum;
    }
}
