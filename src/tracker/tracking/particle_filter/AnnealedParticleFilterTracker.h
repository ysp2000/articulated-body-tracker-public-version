//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_ANNEALEDPARTICLEFILTERTRACKER_H
#define ARTICULATEDBODYTRACKER_ANNEALEDPARTICLEFILTERTRACKER_H

#include <memory>
#include "../../evaluation/Evaluator.h"
#include "../../dynamics/Dynamics.h"
#include "../../sampling/Sampler.h"
#include "../../sampling/ResultExtractor.h"
#include "../Tracker.h"

/**
 *  Annealed Particle Filter Tracker
 *
 *  Suggested reading: Deutscher J., Blake A., Reid I. - Articulated Body Motion Capture by Annealed Particle Filtering
 *  [http://www.robots.ox.ac.uk/~lav/Papers/deutscher_etal_cvpr2000/deutscher_etal_cvpr2000.pdf]
 */
class AnnealedParticleFilterTracker : public Tracker {

public:
    AnnealedParticleFilterTracker(std::unique_ptr<Sampler> &sampler, std::unique_ptr<Dynamics> &dynamics,
                                  std::unique_ptr<Evaluator> &evaluator,
                                  std::unique_ptr<ResultExtractor> &resultExtractor, int numSamples = 500, int numAnnealingIterations = 5);

    void initialize(const TrackingParams &trackingParams, const std::vector<Sample> &initialDistribution) override;

    void track(const cv::Mat &frame) override;

    bool hasPrediction() override;

    bool getPrediction(cv::Mat &prediction) override;

private:
    std::unique_ptr<Sampler> sampler;
    std::unique_ptr<Dynamics> dynamics;
    std::unique_ptr<Evaluator> evaluator;
    std::unique_ptr<ResultExtractor> resultExtractor;

    int numSamples;
    int numAnnealingIterations;

    bool hasPredictionFlag;
    cv::Mat prediction;

    std::unique_ptr<Dynamics> driftDynamics;

    std::vector<Sample> distribution;
    std::vector<Sample> resampledDistribution;
    std::vector<Sample> propagatedDistribution;

    void propagate(const vector<Sample> &currentDistribution, vector<Sample> &updatedDistribution);

    void drift(const vector<Sample> &currentDistribution, vector<Sample> &updatedDistribution);

    static void applyDynamics(const vector<Sample> &currentDistribution, vector<Sample> &updatedDistribution,
                              Dynamics *customDynamics);

    static void normalize(vector<Sample> &distribution);
};


#endif //ARTICULATEDBODYTRACKER_ANNEALEDPARTICLEFILTERTRACKER_H
