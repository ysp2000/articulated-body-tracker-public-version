//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_TRACKERAPP_H
#define ARTICULATEDBODYTRACKER_TRACKERAPP_H

#include <memory>
#include <opencv2/opencv.hpp>
#include "params/TrackingParams.h"
#include "tracking/Tracker.h"
#include "sampling/Sampler.h"
#include "dynamics/Dynamics.h"
#include "evaluation/Evaluator.h"
#include "sampling/ResultExtractor.h"
#include "body/BodyToScreenMapper.h"

class TrackerApp {
public:

    TrackerApp(const TrackingParams &params);

    void initialize();

    void run();

    void release();

private:

    TrackingParams params;

    std::shared_ptr<BodyToScreenMapper> bodyToScreenMapper;
    std::shared_ptr<BodyToStateMapper> bodyToStateMapper;

    VideoCapture videoCapture;
    Mat initialFrame;

    std::unique_ptr<Tracker> tracker;

    void initializeVideoCapture();
    void initializeBodyToScreenMapper();
    void initializeTracker();

    void preprocessFrame(cv::Mat &frame);
    void drawState(cv::Mat &frame, const cv::Mat &state, double alpha = 1.0);
    void drawLimb(cv::Mat &frame, const std::vector<cv::Point2f> &verts, const cv::Scalar &color);

    std::unique_ptr<Sampler> initializeSampler();
    std::unique_ptr<Dynamics> initializeDynamics();
    std::unique_ptr<Evaluator> initializeEvaluator(const cv::Mat &initialFrame);
    std::unique_ptr<ResultExtractor> initializeResultExtractor();
};


#endif //ARTICULATEDBODYTRACKER_TRACKERAPP_H
