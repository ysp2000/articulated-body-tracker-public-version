//
// Copyright (c) 2016 Sergey Esipenko
//

#include "BodyToStateMapper.h"

using  namespace std;
using  namespace cv;

BodyToStateMapper::BodyToStateMapper(int dynamicsModelOrder, const BodyParams &bodyParams) :
        dynamicsModelOrder(dynamicsModelOrder), bodyParams(bodyParams) {}

cv::Mat BodyToStateMapper::getState(std::vector<cv::Point3f> positions, std::vector<cv::Point3f> orientations) {

    Mat matPositions = Mat(positions).reshape(1);
    Mat matOrientations = Mat(orientations).reshape(1);

    Mat state;
    hconcat(matPositions, matOrientations, state);
    state = state.reshape(1, 1);
    vconcat(state, Mat::zeros(dynamicsModelOrder, state.cols, state.type()), state);

    return state;
}

cv::Mat BodyToStateMapper::getInitialState() {
    return getState(bodyParams.getInitialPositions(), bodyParams.getInitialOrientations());
}

std::vector<cv::Point3f> BodyToStateMapper::getPositions(cv::Mat &state) {
    return getLimbsParam<Point3f>(state, LIMB_POSITION_OFFSET, LIMB_POSITION_SIZE);
}

std::vector<cv::Point3f> BodyToStateMapper::getOrientations(cv::Mat &state) {
    return getLimbsParam<Point3f>(state, LIMB_ORIENTATION_OFFSET, LIMB_ORIENTATION_SIZE);
}

template<typename TParam>
std::vector<TParam> BodyToStateMapper::getLimbsParam(cv::Mat &state, size_t paramOffset, size_t paramSize) {
    vector<TParam> limbsParams;
    auto limbs = bodyParams.getLimbs();

    for (size_t limbIndex = 0; limbIndex < limbs.size(); limbIndex++) {
        const size_t limbOffset = LIMB_STATE_SIZE * limbIndex;

        int limbParamStart = int(limbOffset + paramOffset);
        int limbParamEnd = int(limbParamStart + paramSize);

        auto position = TParam(state(Range(0, 1), Range(limbParamStart, limbParamEnd)));
        limbsParams.push_back(position);
    }

    return limbsParams;
}
