//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_BODYSTATEMAPPER_H
#define ARTICULATEDBODYTRACKER_BODYSTATEMAPPER_H

#include <memory>
#include <opencv2/opencv.hpp>
#include "../params/BodyParams.h"

class BodyToStateMapper {

public:
    static const size_t LIMB_POSITION_SIZE = 3;
    static const size_t LIMB_ORIENTATION_SIZE = 3;
    static const size_t LIMB_POSITION_OFFSET = 0;
    static const size_t LIMB_ORIENTATION_OFFSET = LIMB_POSITION_OFFSET + LIMB_POSITION_SIZE;
    static const size_t LIMB_STATE_SIZE = LIMB_POSITION_SIZE + LIMB_ORIENTATION_SIZE;

public:
    BodyToStateMapper(int dynamicsModelOrder, const BodyParams &bodyParams);

    cv::Mat getState(std::vector<cv::Point3f> positions, std::vector<cv::Point3f> orientations);

    cv::Mat getInitialState();

    std::vector<cv::Point3f> getPositions(cv::Mat &state);

    std::vector<cv::Point3f> getOrientations(cv::Mat &state);

private:

    int dynamicsModelOrder;

    BodyParams bodyParams;

    template <typename TParam>
    std::vector<TParam> getLimbsParam(cv::Mat &state, size_t paramOffset, size_t paramSize);

};


#endif //ARTICULATEDBODYTRACKER_BODYSTATEMAPPER_H
