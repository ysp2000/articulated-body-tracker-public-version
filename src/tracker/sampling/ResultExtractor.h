//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_RESULTEXTRACTOR_H
#define ARTICULATEDBODYTRACKER_RESULTEXTRACTOR_H

#include <vector>
#include <opencv2/opencv.hpp>
#include "Sample.h"

class ResultExtractor {
public:

    virtual cv::Mat extract(std::vector<Sample> distribution) = 0;

    virtual ~ResultExtractor() {}
};

#endif //ARTICULATEDBODYTRACKER_RESULTEXTRACTOR_H
