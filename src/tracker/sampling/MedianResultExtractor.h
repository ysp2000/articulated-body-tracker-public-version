//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_MEDIANRESULTEXTRACTOR_H
#define ARTICULATEDBODYTRACKER_MEDIANRESULTEXTRACTOR_H

#include <opencv2/opencv.hpp>
#include "ResultExtractor.h"

class MedianResultExtractor : public ResultExtractor {

public:
    cv::Mat extract(std::vector<Sample> distribution) override;

};


#endif //ARTICULATEDBODYTRACKER_MEDIANRESULTEXTRACTOR_H
