//
// Copyright (c) 2016 Sergey Esipenko
//

#include "MaxWeightResultExtractor.h"

cv::Mat MaxWeightResultExtractor::extract(std::vector<Sample> distribution) {

    if (distribution.empty())
        throw std::invalid_argument("Distribution should be non-empty");

    auto &bestSample = *std::max_element(distribution.begin(), distribution.end(), [](auto &s1, auto &s2) {
        return s1.weight < s2.weight;
    });

    return bestSample.state;
}
