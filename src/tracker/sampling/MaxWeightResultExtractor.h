//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_MAXWEIGHTRESULTEXTRACTOR_H
#define ARTICULATEDBODYTRACKER_MAXWEIGHTRESULTEXTRACTOR_H

#include <opencv2/opencv.hpp>
#include "ResultExtractor.h"

class MaxWeightResultExtractor : public ResultExtractor {

public:
    cv::Mat extract(std::vector<Sample> distribution) override;

};


#endif //ARTICULATEDBODYTRACKER_MAXWEIGHTRESULTEXTRACTOR_H
