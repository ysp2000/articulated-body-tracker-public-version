//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_SAMPLE_H
#define ARTICULATEDBODYTRACKER_SAMPLE_H

#include <opencv2/opencv.hpp>

struct Sample {
    cv::Mat state;
    double weight;

    Sample(const cv::Mat &state, double weight);

    cv::Mat getWeightedState() const;
};

#endif //ARTICULATEDBODYTRACKER_SAMPLE_H
