//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_WEIGHTEDSAMPLER_H
#define ARTICULATEDBODYTRACKER_WEIGHTEDSAMPLER_H

#include <random>

#include "Sampler.h"

class WeightedSampler : public Sampler {
public:
    void sampleDistribution(std::vector<Sample> &weightedDistribution, const int samplesCount,
                            std::vector<Sample> &dst) override;


private:
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution; // default [0-1]

    template <typename T>  static int lowerBound(std::vector<T> a, T x);
};

#endif //ARTICULATEDBODYTRACKER_WEIGHTEDSAMPLER_H
