//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_ISAMPLER_H
#define ARTICULATEDBODYTRACKER_ISAMPLER_H

#include <vector>
#include "Sample.h"

class Sampler {
public:

    virtual void sampleDistribution(std::vector<Sample> &weightedDistribution, const int samplesCount, std::vector<Sample> &dst) = 0;

    virtual ~Sampler() {}
};

#endif //ARTICULATEDBODYTRACKER_ISAMPLER_H
