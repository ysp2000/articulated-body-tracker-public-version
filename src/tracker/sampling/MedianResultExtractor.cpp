//
// Copyright (c) 2016 Sergey Esipenko
//

#include "MedianResultExtractor.h"
#include "../../utils/ImageUtils.h"

using namespace cv;

cv::Mat MedianResultExtractor::extract(std::vector<Sample> distribution) {

    if (distribution.empty())
        throw std::invalid_argument("Distribution should be non-empty");

    Mat mean = ImageUtils::zeros_like(distribution[0].state);

    for (auto &sample : distribution) {
        mean += sample.getWeightedState();
    }

    return mean;
}
