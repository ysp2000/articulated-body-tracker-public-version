//
// Copyright (c) 2016 Sergey Esipenko
//

#include "WeightedSampler.h"

static const double EPSILON = 1e-7;

void WeightedSampler::sampleDistribution(std::vector<Sample> &weightedDistribution, const int samplesCount,
                                         std::vector<Sample> &dst) {

    std::vector<double> cumulativeProbability({0.0});

    // cumulative sum
    for (auto &sample : weightedDistribution) {
        cumulativeProbability.push_back(sample.weight + cumulativeProbability.back());
    }

    // normalization
    for (auto &p : cumulativeProbability) {
        p /= cumulativeProbability.back();
    }

    // slightly move boundaries for robust binary search
    cumulativeProbability.front() -= EPSILON;
    cumulativeProbability.back() += EPSILON;

    // sampling
    std::vector<Sample> sampledDistribution;
    for (int it = 0; it < samplesCount; it++) {
        const int idx = lowerBound(cumulativeProbability, distribution(generator));
        const Sample &sample = weightedDistribution[idx];
        sampledDistribution.push_back(sample);
    }

    dst.clear();
    dst.insert(dst.end(), sampledDistribution.begin(), sampledDistribution.end());
}

template<typename T>
int WeightedSampler::lowerBound(std::vector<T> a, T x) {
    int left = 0;
    int right = (int)a.size() - 1;

    int lowerBound = left;

    while (left <= right) {
        const int middle = left + (right - left) / 2;
        if (a[middle] <= x) {
            left = middle + 1;
            lowerBound = std::max(lowerBound, middle);
        } else {
            right = middle - 1;
        }
    }

    return lowerBound;
}
