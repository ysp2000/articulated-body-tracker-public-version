//
// Copyright (c) 2016 Sergey Esipenko
//

#include "Sample.h"

Sample::Sample(const cv::Mat &state, double weight) : state(state), weight(weight) {}

cv::Mat Sample::getWeightedState() const {
    return state * weight;
}
