//
// Copyright (c) 2016 Sergey Esipenko
//

#include "BodyToScreenMapper.h"

using namespace std;
using namespace cv;

BodyToScreenMapper::BodyToScreenMapper(const BodyParams &bodyParams,
                                       const shared_ptr<BodyToStateMapper> &bodyToStateMapper,
                                       const shared_ptr<ScreenProjector> &screenProjector) :
        bodyParams(bodyParams), bodyToStateMapper(bodyToStateMapper), screenProjector(screenProjector) {}

void BodyToScreenMapper::getLimbsVertices(cv::Mat state, std::vector<std::vector<cv::Point2f>> &limbsVerts) {

    auto limbs = bodyParams.getLimbs();
    auto limbPositions = bodyToStateMapper->getPositions(state);
    auto limbOrientations = bodyToStateMapper->getOrientations(state);

    std::vector<Mat> limbTransformations;

    for (int limbIndex = 0; limbIndex < limbs.size(); limbIndex++) {

        Limb &limb = limbs[limbIndex];

        auto position = limbPositions[limbIndex];
        auto orientation = limbOrientations[limbIndex];

        Mat parentTransformation = limb.parent >= 0 ? limbTransformations[limb.parent] : cv::Mat::eye(4, 4, CV_32F);

        Mat limbVerts, limbTransform;
        std::tie(limbVerts, limbTransform) = limb.transform(parentTransformation, position, orientation);

        limbTransformations.push_back(limbTransform);
        limbsVerts.push_back(screenProjector->getProjection(limbVerts));
    }
}

void BodyToScreenMapper::extractLimbsImages(std::vector<cv::Mat> &dst, const cv::Mat &frame, const cv::Mat &state,
                                            const double scaleFactor) {
    std::vector<std::vector<Point2f>> stateVerts;
    getLimbsVertices(state, stateVerts);

    auto limbs = bodyParams.getLimbs();
    dst.clear();

    for (int limbIdx = 0; limbIdx < stateVerts.size(); limbIdx++) {
        auto &limb = limbs[limbIdx];
        auto &limbVerts = stateVerts[limbIdx];
        Mat limbImage(int(ceil(limb.length * scaleFactor + 1)),
                      int(ceil(limb.radius * 2 * scaleFactor + 1)),
                      frame.type());
        ScreenProjector::getBackProjection(frame, limbVerts, limbImage);
        dst.push_back(limbImage);
    }
}

std::vector<cv::Mat> BodyToScreenMapper::getLimbsImages(cv::Mat &frame, cv::Mat &state, const double scaleFactor) {
    std::vector<Mat> limbsImages;
    extractLimbsImages(limbsImages, frame, state, scaleFactor);
    return limbsImages;
}
