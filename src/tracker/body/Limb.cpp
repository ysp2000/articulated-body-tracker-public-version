//
// Copyright (c) 2016 Sergey Esipenko
//

#include "Limb.h"
#include "../../math/SpaceTransformations.h"

using namespace cv;

Limb::Limb(int index, int parent, float length, float radius, const Scalar &color) : index(index), parent(parent),
                                                                                     length(length), radius(radius),
                                                                                     color(color) {
    verts = (Mat_<float>(4, 4) <<
            -radius, 0.f, 0.f, 1.f,
            -radius, length, 0.f, 1.f,
            +radius, length, 0.f, 1.f,
            +radius, 0.f, 0.f, 1.f);
}

std::tuple<Mat, Mat> Limb::transform(Mat parentTransform, Point3f position, Point3f orientation) {

    Mat limbOriginTranslation = SpaceTransformations::getTranslationMatrix(position);
    Mat limbRotation = SpaceTransformations::getRotationMatrix(orientation);
    Mat limbEndTranslation = SpaceTransformations::getTranslationMatrix(Point3f(0.f, length, 0.f));

    Mat limbTransform = limbRotation * limbOriginTranslation * parentTransform;

    Mat worldVerts = verts * limbTransform;
    Mat limbEndTransform = limbEndTranslation * limbTransform;

    return std::make_tuple(worldVerts, limbEndTransform);
}
