//initialDistribution
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_LIMB_H
#define ARTICULATEDBODYTRACKER_LIMB_H

#include "opencv2/opencv.hpp"



struct Limb {
    int index;
    int parent;
    float length;
    float radius;
    cv::Scalar color;
    cv::Mat verts;

    Limb(int index, int parent, float length, float radius, const cv::Scalar &color);

    std::tuple<cv::Mat, cv::Mat> transform(cv::Mat parentTransform, cv::Point3f position, cv::Point3f orientation);
};


#endif //ARTICULATEDBODYTRACKER_LIMB_H
