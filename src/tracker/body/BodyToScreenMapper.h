//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_LIMBTOSCREENMAPPER_H
#define ARTICULATEDBODYTRACKER_LIMBTOSCREENMAPPER_H

#include <memory>
#include <vector>
#include <opencv2/opencv.hpp>
#include <bits/shared_ptr.h>

#include "../../math/ScreenProjector.h"
#include "../params/BodyParams.h"
#include "../state/BodyToStateMapper.h"

class BodyToScreenMapper {

public:

    BodyToScreenMapper(const BodyParams &bodyParams,
                       const std::shared_ptr<BodyToStateMapper> &bodyToStateMapper,
                       const std::shared_ptr<ScreenProjector> &screenProjector);

    void getLimbsVertices(cv::Mat state, std::vector<std::vector<cv::Point2f>> &limbsVerts);

    void extractLimbsImages(std::vector<cv::Mat>& dst, const cv::Mat& frame, const cv::Mat& state, const double scaleFactor = 1.0);

    std::vector<cv::Mat> getLimbsImages(cv::Mat& frame, cv::Mat& state, const double scaleFactor = 1.0);

private:
    BodyParams bodyParams;
    std::shared_ptr<BodyToStateMapper> bodyToStateMapper;
    std::shared_ptr<ScreenProjector> screenProjector;
};


#endif //ARTICULATEDBODYTRACKER_LIMBTOSCREENMAPPER_H
