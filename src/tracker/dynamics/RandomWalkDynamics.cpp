//
// Copyright (c) 2016 Sergey Esipenko
//

#include "RandomWalkDynamics.h"

using namespace cv;

RandomWalkDynamics::RandomWalkDynamics(const cv::Mat &standardDeviation) : standardDeviation(standardDeviation) {}

cv::Mat RandomWalkDynamics::simulate(cv::Mat &state) {

    Mat updatedState = Mat::zeros(state.size(), state.type());
    randn(updatedState, Scalar::all(0.0), Scalar::all(1.0));
    multiply(updatedState, standardDeviation, updatedState);
    updatedState += state;

    return updatedState;
}
