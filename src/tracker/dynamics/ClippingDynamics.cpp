//
// Copyright (c) 2016 Sergey Esipenko
//

#include "ClippingDynamics.h"

using namespace cv;

ClippingDynamics::ClippingDynamics(const Mat &stateParamsMin, const Mat &stateParamsMax) : stateParamsMin(
        stateParamsMin), stateParamsMax(stateParamsMax) {}

cv::Mat ClippingDynamics::simulate(cv::Mat &state) {

    cv::Mat clippedState = state.clone();
    clippedState = cv::max(stateParamsMin, clippedState);
    clippedState = cv::min(stateParamsMax, clippedState);

    return clippedState;
}
