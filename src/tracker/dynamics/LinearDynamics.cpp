//
// Copyright (c) 2016 Sergey Esipenko
//

#include "LinearDynamics.h"

using namespace cv;

LinearDynamics::LinearDynamics(double attenuationFactor) : attenuationFactor(attenuationFactor) {}

cv::Mat LinearDynamics::simulate(cv::Mat &state) {

    Mat updatedState = state.clone();

    // Increment state variables and their derivatives by according derivatives of higher order.
    // Derivatives of the highest order will stay untouched.
    Mat stateDerivative = state.rowRange(1, state.rows);
    updatedState.rowRange(0, stateDerivative.rows) += stateDerivative;

    // Simulate the highest order derivative attenuation
    updatedState.row(updatedState.rows - 1) *= attenuationFactor;

    return updatedState;
}
