//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_LINEARDYNAMICS_H
#define ARTICULATEDBODYTRACKER_LINEARDYNAMICS_H


#include "Dynamics.h"

class LinearDynamics : public Dynamics {

public:
    LinearDynamics(double attenuationFactor = 1.0);

    cv::Mat simulate(cv::Mat &state) override;

private:
    double attenuationFactor;
};


#endif //ARTICULATEDBODYTRACKER_LINEARDYNAMICS_H
