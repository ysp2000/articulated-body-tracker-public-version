//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_RANDOMWALKDYNAMICS_H
#define ARTICULATEDBODYTRACKER_RANDOMWALKDYNAMICS_H

#include <opencv2/opencv.hpp>

#include "Dynamics.h"

class RandomWalkDynamics : public Dynamics {
public:

    RandomWalkDynamics(const cv::Mat &standardDeviation);

    cv::Mat simulate(cv::Mat &state) override;

private:
    cv::Mat standardDeviation;
};


#endif //ARTICULATEDBODYTRACKER_RANDOMWALKDYNAMICS_H
