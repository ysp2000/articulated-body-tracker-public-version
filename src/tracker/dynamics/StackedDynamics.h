//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_STACKEDDYNAMICS_H
#define ARTICULATEDBODYTRACKER_STACKEDDYNAMICS_H


#include <list>
#include <memory>
#include <opencv2/opencv.hpp>
#include "Dynamics.h"

class StackedDynamics : public Dynamics {
public:

    void addToDynamicsChain(std::unique_ptr<Dynamics> &dynamics);

    cv::Mat simulate(cv::Mat &state) override;

private:
    std::list<std::unique_ptr<Dynamics>> processingChain;
};


#endif //ARTICULATEDBODYTRACKER_STACKEDDYNAMICS_H
