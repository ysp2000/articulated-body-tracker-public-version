//
// Copyright (c) 2016 Sergey Esipenko
//

#include "StackedDynamics.h"

using namespace cv;

void StackedDynamics::addToDynamicsChain(std::unique_ptr<Dynamics> &dynamics) {
    processingChain.push_back(std::move(dynamics));
}

cv::Mat StackedDynamics::simulate(cv::Mat &state) {

    cv::Mat updatedState = state;

    for (auto &dynamics : processingChain) {
        updatedState = dynamics->simulate(updatedState);
    }

    return updatedState;
}
