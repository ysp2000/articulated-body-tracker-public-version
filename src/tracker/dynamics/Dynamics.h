//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_DYNAMICS_H
#define ARTICULATEDBODYTRACKER_DYNAMICS_H

#include <opencv2/opencv.hpp>

class Dynamics {
public:

    virtual cv::Mat simulate(cv::Mat &state) = 0;

    virtual ~Dynamics() {};

};

#endif //ARTICULATEDBODYTRACKER_DYNAMICS_H
