//
// Copyright (c) 2016 Sergey Esipenko
//

#ifndef ARTICULATEDBODYTRACKER_CLIPPINGDYNAMICS_H
#define ARTICULATEDBODYTRACKER_CLIPPINGDYNAMICS_H

#include <opencv2/opencv.hpp>
#include "Dynamics.h"

class ClippingDynamics : public Dynamics {
public:

    ClippingDynamics(const cv::Mat &stateParamsMin, const cv::Mat &stateParamsMax);

    cv::Mat simulate(cv::Mat &state) override;

private:
    cv::Mat stateParamsMin;
    cv::Mat stateParamsMax;
};


#endif //ARTICULATEDBODYTRACKER_CLIPPINGDYNAMICS_H
